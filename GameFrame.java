import java.awt.Frame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame extends Frame {
	Karta karta;
	
	public void paint(Graphics g) {
		BufferedImage img = null;
		File f = new File("Hero.jpg");
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g.drawImage(img, 40 + karta.hero.posx * 50, 40 + karta.hero.posy * 50, null);
		for (int i = 0; i < karta.sizex; i++) {
			for (int j = 0; j < karta.sizey; j++) {
				g.drawRect(40 + i * 50, 40 + j * 50,  50,  50);
			}
		}
	}
}
