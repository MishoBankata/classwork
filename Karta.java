public class Karta {
	int sizex;
	int sizey;
	Hero hero;
	
	Karta(int x, int y, Hero h) {
		sizex = x;
		sizey = y;
		hero = h;
	}
	
	void print() {
		for (int i = 0; i < sizey; i++) {
			for (int j = 0; j < sizex; j++) {
				if (hero.posx == j && hero.posy == i) {
					System.out.print("X ");
				} else {
					System.out.print(". ");
				}
			}
			System.out.println();
		}
	}
}
